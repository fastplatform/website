# `website`: [fastplatform.eu](https://fastplatform.eu) website

This repository contains the source code of the [fastplatform.eu](https://fastplatform.eu) project website.

The website is written with [Jekyll](https://jekyllrb.com/). The `master` branch is built and published by [Netlify](https://www.netlify.com/), on every commit.

## Development

To run the service on your local machine, ensure you have the following dependencies installed:
- [Ruby](https://www.ruby-lang.org/en/)
- [RubyGems](https://rubygems.org/)

Install the Jekyll libraries from the `Gemfile` file:
```bash
gem install bundler
bundle install
```

Run the local server:
```bash
bundle exec jekyll serve

# Or `make start`
```

The website server is now started and available at http://localhost:4000.
